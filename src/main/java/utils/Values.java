package utils;

public class Values {

    public static final int PACKET_GENERATE_QUANTITY = 5;
    public static final int PACKET_GENERATE_DELAY = 750;
    public static final int NUMBER_OF_THREADS = 5;
    public static final byte B_SRC = (byte) 127;

    public static final String PRODUCT_NAME_COMMAND_PARAM = "productName";
    public static final String GROUP_NAME_COMMAND_PARAM = "groupName";
    public static final String QUANTITY_COMMAND_PARAM = "quantity";
    public static final String PRICE_COMMAND_PARAM = "price";
    public static final String ERROR_MESSAGE_COMMAND_PARAM = "errorMessage";

    public static final String RESPONSE_STATUS_COMMAND_PARAM = "responseStatus";
    public static final String RESPONSE_STATUS_OK = "Ok";
    public static final String RESPONSE_STATUS_ERROR = "Error";

}
